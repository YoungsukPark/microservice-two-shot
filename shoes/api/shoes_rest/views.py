from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO

# Create your views here.

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "manufacturer",
        "model_name",
        "color",
        "url",
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "closet_name", "bin_number", "bin_size"]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "manufacturer",
        "model_name",
        "color",
        "url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }



@require_http_methods(["GET", "POST"])
def list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()

        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"Message": "Invalid Bin"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )



@require_http_methods(["DELETE", "GET"])
def detail_shoes(request, pk):

    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"Message": "Shoe does not exist."})
            response.status_code = 404
            return response

    else:
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeListEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"Message": "Shoe does not exist."})
