# Wardrobify

Team:

- Chase Whitaker - Hats Microservice

* Youngsuk Park - Shoes microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

I would begin by installing Django app into the Django project. Then I would proceed by making my models, views, and urls. I would probably
use GET, POST, PUT, DELETE methods to modify these in insomnia. I would constantly check on insomnia to see if any of the features/functions
are broken. Everytime a test passes, I would git push/pull/merge with main including my partner's work.

## Hats microservice

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Explain your models and integration with the wardrobe
microservice, here.

That hats microservice and the shoes microservices were created to be very similar.
The Hats API contains two main elements, the Poll/Poller and the Django project.

Backend Stuff:
The API microservice is where the hats_project and hats_rest exist. The hats_rest is where the views, models, and urls are that allow users to create hats, delete hats, and view their hats list (but not really this is just the backend so it allows the user to do that, but the user doesn't see this part.) The hats that are created are placed into additional locations, for tracking purposes, from the LocationVO model.

The Poll application has the poller function, this checks the Location data from the wardrobe API on a minute interval. The poll can create/update the objects within any given LocationVO.

Frontend Stuff:
Using React, we render a single page application to show various elements of our microservice apps given a users input. From the main page, users can see an option to add more hats, that takes them to a hat form, which takes the user's inputs and creates a new hat for their hat list. The main page also features a button to view the hats list , where users have the option to delete any hat from their list. To make the application work as a SPA, we used the react-router-dom to add BrowserRoutes, Routes, and Route to our App.js. We used the react-router-dom elements to create paths for the HatList and the HatForm so users could access the pages without having to type in a specific url.

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Details about backend components:

Models for hats:
Hat:
_ fabric
_ style_name
_ color
_ picture_url \* location from a foreign key

LocationVO:
_ import_href
_ closet_name
_ section_number
_ shelf_number

RESTful APIs (port 8090)
METHOD || URL || PURPOSE || VIEW ||
GET || /api/hats || Lists hats || list_hats ||
POST || /api/hats/ || Create hat || list_hats ||
GET || /api/hats/<int:pk>/ || Hat details || detail_hats ||
DELETE || /api/hats/<int:pk>/ || Delete hat || detail_hats ||

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
