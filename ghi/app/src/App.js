import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './ShoeForm';
import ShoesList from './ListShoe';
import HatForm from './HatForm';
import HatList from './HatList';



  // if (props.shoes === undefined) {

function App(props) {
  if (props.hats === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="new" element={<ShoeForm />} />
          </Route>
          <Route path="shoes">
            <Route path="list" element={<ShoesList shoes={props.shoes} />} />
          </Route>
          <Route path="/hats/new" element={<HatForm />} ></Route>
          <Route path="/hats" element={<HatList hats={props.hats} />} ></Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
