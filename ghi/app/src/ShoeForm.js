import React, { useEffect, useState } from 'react';

function ShoeForm() {
    const [bins, setBins] = useState([]);

    const [name, setName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [modelName, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [url, setUrl] = useState('');
    const [bin, setBin] = useState('');


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handleUrlChange = (event) => {
        const value = event.target.value;
        setUrl(value);
    }
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.url = url;
        data.bin = bin;
        console.log(data);

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const shoeResponse = await fetch(shoeUrl, fetchConfig);
        if (shoeResponse.ok) {
            const newShoe = await shoeResponse.json();
            console.log(newShoe);

            setName('');
            setManufacturer('');
            setModelName('');
            setColor('');
            setUrl('');
            setBin('');
        }
    }

    const fetchData = async () => {
        const binUrl = 'http://localhost:8100/api/bins/';

        const binResponse = await fetch(binUrl);

        if (binResponse.ok) {
            const data = await binResponse.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Shoe</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleModelNameChange} value={modelName} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
                    <label htmlFor="model_name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleUrlChange} value={url} placeholder="Url" required type="text" name="url" id="url" className="form-control" />
                    <label htmlFor="url">Url</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleBinChange} value={bin} required id="bin" name="bin" className="form-select">
                        <option value="">Choose a Bin</option>
                        {bins.map(bin => {
                            return(
                                <option key={bin.href} value={bin.href}>
                                    {bin.closet_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
        </div>
    );

}

export default ShoeForm;
