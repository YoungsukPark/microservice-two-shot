import React from 'react';
function HatList(props) {
//     return (
//         <table className="table table-striped table bordered table-hover">
//             <thead>
//                 <tr>
//                     <th>Style Name</th>
//                     <th>Fabric</th>
//                     <th>Color</th>
//                     <th>Location</th>
//                     <th>URL</th>
//                 </tr>
//             </thead>
//                 <tbody>
//                 {props.hats.map(hat => {
//                     return (
//                         <tr key={hat.href}>
//                             <td>{hat.style_name}</td>
//                             <td>{hat.fabric}</td>
//                             <td>{hat.color}</td>
//                             <td>{hat.location.closet_name}</td>
//                             <td>{hat.picture_url}</td>
//                         </tr>
//                     )
//                 })}
//             </tbody>
//         </table>
//     );
// }



    async function deleteHat(hat) {
        const deleteUrl = `http://localhost:8090${hat.href}`
        const deleteResponse = await fetch(deleteUrl, {method: 'delete'})
        if (deleteResponse.ok) {
            console.log(`You just deleted ${hat.style_name}`)
            alert(`You just deleted ${hat.style_name}`)
        }
    }
    return (
        <div className="text-center">
            {props.hats.map(hat => {
            return(
                <div key={hat.href} className="card mb-3 shadow">
                <img src={hat.picture_url} className="card-image-top w-50 mx-auto" />
                <div className="card-body">
                    <h5 className="card-title">{hat.style_name}</h5>
                    <h6 className="card-subtitle mb-2 text-muted">{hat.fabric}</h6>
                    <p className="card-text">Color - {hat.color}</p>
                    <p className="card-text">Stored in - {hat.location.closet_name}</p>
                    <button type="button" className="btn btn-outline-danger" onClick={() => deleteHat(hat)}>Delete Forever</button>
                </div>
                </div>
            );
            })}
        </div>
    );
}

export default HatList;
