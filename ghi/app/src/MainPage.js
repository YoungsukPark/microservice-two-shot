import { Link } from 'react-router-dom';

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </p>
        <div className="column">
          <div className="card mb-3 shadow">
              <img className="card-img-top p-3 mx-auto" src="https://images.pexels.com/photos/2442889/pexels-photo-2442889.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2" alt="..."/>
              <Link to="/shoes/list/" className="btn btn-sm btn-info">Click me to see your shoes!</Link>
          </div>
        </div>
        <div className="column">
          <div className="card mb-3 shadow">
            <img className="card-img-top p-3 mx-auto" src="https://thumbs.dreamstime.com/b/pile-straw-hats-big-sale-hanging-rack-124945587.jpg" alt="..." />
            <Link to="/hats/" className="btn btn-sm btn-info">Click me to see your hats!</Link>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
