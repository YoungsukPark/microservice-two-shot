


// function ShoesList(props) {
//     return(
//         <table className="table table-dark table-striped table-hover">
//             <thead>
//                 <tr>
//                     <th>Shoes</th>
//                     <th>Color</th>
//                     <th>Manufacturer</th>
//                     <th>Model Name</th>
//                     <th>Bin</th>
//                     <th>Url</th>
//                     <th>Delete</th>
//                 </tr>
//             </thead>
//             <tbody>
//             {props.shoes.map(shoe => {
//                 return (
//                     <tr key={ shoe.href }>
//                     <td>{ shoe.name }</td>
//                     <td>{ shoe.color }</td>
//                     <td>{ shoe.manufacturer }</td>
//                     <td>{ shoe.model_name }</td>
//                     <td>{ shoe.bin }</td>
//                     <td>{ shoe.url }</td>
//                     <td>
//                         <button type="button" className="btn btn-outline-danger" onClick={() => this.delete(shoe.href)}>Delete</button>
//                     </td>
//                     </tr>
//                 );
//             })}
//             </tbody>
//         </table>
//     );
// }


// export default ShoesList;


import React from 'react';

function ShoesList(props) {

    async function deleteShoe(shoe) {
        const deleteUrl = `http://localhost:8080${shoe.href}`
        const deleteResponse = await fetch(deleteUrl, {method: 'delete'})
        if (deleteResponse.ok) {
            console.log(`You just deleted : ${shoe.name}`)
            alert(`You just deleted : ${shoe.name}`)
        }
    }

    return (
    <div className="text-center">
        {props.shoes.map(shoe => {
        return (
            <div key={shoe.href} className="card mb-3 shadow">
            <img src={shoe.url} className="card-img-top w-50 p-3 mx-auto" />
            <div className="card-body">
                <h5 className="card-title">{shoe.name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">
                {shoe.model_name}
                </h6>
                <p className="card-text">
                Manufacturer - {shoe.manufacturer}
                </p>
                <p className="card-text">
                Stored in - {shoe.bin}
                </p>
                <p className="card-text">
                Color - {shoe.color}
                </p>
                <button type="button" className="btn btn-outline-danger" onClick={() => deleteShoe(shoe)}>
                    Delete
                </button>
            </div>
            </div>
        );
        })}
    </div>
    );
}

export default ShoesList;
