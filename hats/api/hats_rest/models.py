from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()


class Hat(models.Model):
    fabric = models.CharField(max_length =150)
    style_name = models.CharField(max_length =150)
    color = models.CharField(max_length =150)
    picture_url = models.URLField(max_length =300)
    location = models.ForeignKey(
        "LocationVO",
        related_name="hats",
        on_delete=models.CASCADE,
        null = True,
    )
    def __str__(self):
        return self.style_name

    def get_api_url(self):
        return reverse('detail_hats', kwargs={"pk" : self.pk})

    class Meta:
        ordering = ("fabric", "style_name", "color", "picture_url", "location")
