from django.urls import path

from .views import list_hats, detail_hats

urlpatterns =[
    path("hats/", list_hats, name="create_list_hats"),
    path("locations/<int:location_vo_id>/hats/", list_hats, name="list_hats"),
    path("hats/<int:pk>/", detail_hats, name="detail_hats"),

]
