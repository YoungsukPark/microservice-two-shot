# Generated by Django 4.0.3 on 2023-03-03 06:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0004_alter_hat_picture_url'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='hat',
            options={'ordering': ('fabric', 'style_name', 'color', 'picture_url', 'location')},
        ),
    ]
